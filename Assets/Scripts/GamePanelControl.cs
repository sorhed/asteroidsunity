﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class GamePanelControl : MonoBehaviour
    {

        [SerializeField] private Sprite n1, n2, n3, n4, n5, n6, n7, n8, n9, n0;
        [SerializeField] private GameObject numField, gOverPanel, gamePanel;
        [SerializeField] private Image i1, i2, i3, i4, i5, i6, i7;
        [SerializeField] private Animator gOverAnim;
        [SerializeField] private InputField nameInput;
        private Dictionary<int, Sprite> spriteMap;
        private Image numIm;
        private Image[] nums;
        private ScoreComponent statComponent;
        private int LivesClamped
        {
            get { return Mathf.Clamp(statComponent.Lives, 0, 9); }
        }
        private int Score { get { return statComponent.Score; } }
        private int oldLives, oldScore;

        void Awake()
        {
            spriteMap = new Dictionary<int, Sprite>
            {
                {1, n1},
                {2, n2},
                {3, n3},
                {4, n4},
                {5, n5},
                {6, n6},
                {7, n7},
                {8, n8},
                {9, n9},
                {0, n0}
            };
            EventDispatcher.Instance.AddListener(GameOver, Message.GameOver);
            EventDispatcher.Instance.AddListener(x => gamePanel.SetActive(true), Message.GameStarted);
            EventDispatcher.Instance.AddListener(x => gOverPanel.SetActive(false), Message.GameStarted); //z jakiegoś powodu się nie dezaktywuje przy korutynie
            gOverPanel.SetActive(false);
            statComponent = FindObjectOfType<ScoreComponent>();
            nums = new[] {i1, i2, i3, i4, i5, i6, i7};
            numIm = numField.GetComponent<Image>();
            oldLives = LivesClamped;
            oldScore = Score;
        }

        // Update is called once per frame
        void Update () {
            if (oldLives != LivesClamped)
            {
                numIm.sprite = spriteMap[LivesClamped];
                oldLives = LivesClamped;
            }

            if (oldScore != Score)
            {
                RefreshScore();
                oldScore = Score;
            }
        }

        private void RefreshScore()
        {
            var score = GetIntArray(Score);
            var offset = score.Length - 1;

            foreach (var num in nums)
            {
                num.sprite = spriteMap[0];
            }

            for (int i = 0; i < nums.Length; i++)
            {
                if (i > score.Length - 1)
                    return;
                            
                nums[offset-i].sprite = spriteMap[score[i]];
            }
        }

        private int[] GetIntArray(int num)
        {
            //przesuwam przecinek, potem odwracam listę. lepsze niż zamiana na sring
            List<int> listOfInts = new List<int>();
            while (num > 0)
            {
                listOfInts.Add(num % 10);
                num = num / 10;
            }
            listOfInts.Reverse();
            return listOfInts.ToArray();
        }

        private void GameOver(GameObject gObject = null)
        {
            StartCoroutine(CorHelper.PopIn(gOverAnim, gOverPanel));
        }

        /*
        private IEnumerator ShowPanel()
        {
            gOverPanel.SetActive(true);
            gOverAnim.Play("PopUp");
            yield return new WaitForSeconds(60/2.8f);
        }

        private IEnumerator HidePanel()
        {
            gOverAnim.Play("PopOut");
            yield return new WaitForSeconds(60/2.8f);
            gOverPanel.SetActive(false);
        }
        */


        public void OnNameEnter()
        {
            // placeholder save method 
            var text = nameInput.text;
            var obj = new HighScore(Score, text);
            var scores = File.ReadAllLines(Application.dataPath + "/HighScores/scores.txt").ToList();
            var hscores = new List<HighScore>();
            scores.ForEach(s => hscores.Add(JsonUtility.FromJson<HighScore>(s)));
            hscores.Add(obj);
            hscores =  hscores.OrderByDescending(hs => hs.ScoreValue).ToList();
            var strArr = new string[hscores.Count];
            int i = 0;
            foreach (var ha in hscores)
            {
                var serial = JsonUtility.ToJson(ha);
                strArr[i++] = serial;
            }
            File.WriteAllLines(Application.dataPath + "/HighScores/scores.txt", strArr);
            StartCoroutine(EndGame());
            OnGameEnded();
        }

        private IEnumerator EndGame()
        {
            StartCoroutine(CorHelper.PopOut(gOverAnim, gOverPanel));
            yield return new WaitForSeconds(0.5f);
            OnGameEnded();
        }

        private void OnGameEnded()
        {
            Debug.Log("OnGameEnded Sent");
          EventDispatcher.Instance.RaiseEvent(Message.GameEnded, null);
        }
    }
    [Serializable]
    public class HighScore
    {
        public string ScoreValue;
        public string PlayerName;

        public HighScore(int scoreValue, string playerName)
        {
            ScoreValue = scoreValue.ToString();
            PlayerName = playerName;
        }

        public override string ToString()
        {
            return  PlayerName + " - " + ScoreValue;
        }
    }
}
