﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public static class CameraExt {

        public static float MaxX(this Camera camera)
        {
            var size = camera.orthographicSize;

            return camera.transform.position.x + size;
        }

        public static float MaxY(this Camera camera)
        {
            var size = camera.orthographicSize;

            return camera.transform.position.y + size;
        }

        public static float MinX(this Camera camera)
        {
            var size = camera.orthographicSize;

            return camera.transform.position.x - size;
        }

        public static float MinY(this Camera camera)
        {
            var size = camera.orthographicSize;

            return camera.transform.position.y - size;
        }
    }

    public static class Rand 
    {
        private static readonly System.Random rand = new System.Random();

        public static bool CoinFlip()
        {
            return rand.Next() % 2 == 0;
        }

        public static double RandomDouble(double minVal, double maxVal)
        {
            return rand.NextDouble() * (maxVal - minVal) + minVal;
        }

        public static int RandomInt(int minVal, int maxVal)
        {
            return rand.Next(minVal, maxVal);
        }
    }
}