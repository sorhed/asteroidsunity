﻿using System;
using UnityEngine;
using UnityStandardAssets.Utility.Inspector;

namespace Assets.Scripts
{
    public class Asteroid : MonoBehaviour
    {
        public float Acceleration;
        public RockType RockType;
        public Vector3 Direction;
        private float velocity;
        public Vector3 ColliderVRight;
 

        void Awake()
        {
            if (RockType == RockType.Big)
                Direction = new Vector3((float)Rand.RandomDouble(-1,1), (float)Rand.RandomDouble(-1,1),0);
        }

        void InitDirection()
        {
            if (RockType == RockType.Big)
                Direction = new Vector3((float)Rand.RandomDouble(-1, 1), (float)Rand.RandomDouble(-1, 1), 0);
        }

        void Update()
        {
            transform.position += Direction*Acceleration*Time.deltaTime;
        }

        void OnTriggerEnter2D(Collider2D coll)
        {
            if (coll.gameObject.GetComponent<ShotProp>() != null)
            {
                ColliderVRight = coll.transform.right;
                Destroy(gameObject);
                EventDispatcher.Instance.RaiseEvent(Message.RockDestroyed, gameObject);
            }
        }
    }
}
