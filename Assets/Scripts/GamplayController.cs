﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts
{
    public class GamplayController : MonoBehaviour
    {
        [SerializeField] private GameObject BigAss, MedAss, SmallAss, PartExplosion, BigU, SmallU, SpawnPoint1, SpawnPoint2, playerPrefab;
        private PlayerProps player;
        private Animator playerAnim;
        private BoxCollider2D playerBox;
        public List<Asteroid> asteroids = new List<Asteroid>();
        public List<Ufo> ufos = new List<Ufo>();
        private Vector3[] asteroidsStartingPos;

        private int ActiveAstCount
        {
            get { return asteroids.Count(a => a.gameObject.activeSelf); }
        }

        void Awake()
        {
            //player = FindObjectOfType<PlayerProps>();
            //playerAnim = player.gameObject.GetComponent<Animator>();
            asteroidsStartingPos = new[]
            {
                new Vector3(-2.4f, 1.5f, 1),
                new Vector3(-1.72f, -1.9f, 1),
                new Vector3(2.1f, 1.12f, 1),
                new Vector3(0.9f, -1.9f, 1)
            };
            asteroids.AddRange(PopulateFirstWave());
            EventDispatcher.Instance.AddListener(AsteroidDestroyed, Message.RockDestroyed);
            EventDispatcher.Instance.AddListener(BigUfoSpawn, Message.BigUfoSpawn);
            EventDispatcher.Instance.AddListener(SmallUfoSpawn, Message.SmallUfoSpawn);
            EventDispatcher.Instance.AddListener(UfoDestroyed, Message.UfoDestroyed);
            EventDispatcher.Instance.AddListener(PlayerDestroyed, Message.PlayerDestroyed);
            EventDispatcher.Instance.AddListener(GameOver, Message.GameOver);
            EventDispatcher.Instance.AddListener(OnStart, Message.GameStarted);
        }

        public void GameplayInit(GameObject hack = null)
        { 
        }



        private IEnumerable<Asteroid> PopulateFirstWave()
        {
            foreach (Vector3 position in asteroidsStartingPos)
            {
                var a = Instantiate(BigAss, position, Quaternion.identity) as GameObject;
                yield return a.GetComponent<Asteroid>();
            }
        } 

        public void AsteroidDestroyed(GameObject gObject)
        {
            var a = gObject.GetComponent<Asteroid>();
            if (asteroids.Contains(a))
            {
                StartCoroutine(Explosion(gObject.transform.position));
                asteroids.Remove(a);
            }
            else
                Debug.Log("asteroid was not on the track list");

            if (a.RockType == RockType.Big || a.RockType == RockType.Medium)
                Divide(a);                    
        }

        private void Divide(Asteroid asteroid)
        {
            if (asteroid.RockType == RockType.Small)
                throw new ArgumentException("Wrong RockType");

            //instead of simply turning it innactive there should be a coroutine with cool explosion and shit
            for (int i = 0; i < 2; i++)
            {
                var rock =
                    Instantiate(asteroid.RockType == RockType.Big ? MedAss : SmallAss, asteroid.transform.position,
                        Quaternion.identity) as GameObject;

                if (rock != null) asteroids.Add(rock.GetComponent<Asteroid>()); else Debug.Log("Rock == null");
                rock.GetComponent<Asteroid>().Direction = i == 0 ? (asteroid.Direction - asteroid.ColliderVRight).normalized : (asteroid.Direction - -asteroid.ColliderVRight).normalized;
            }
        }

		private void UfoDestroyed(GameObject gObject)
		{
			var u = gObject.GetComponent<Ufo> ();
			if (ufos.Contains (u))
            {
				ufos.Remove (u);
				StartCoroutine (Explosion (gObject.transform.position));
			} else 
				Debug.Log ("Ufo was not on a track list");		
		}

        private void PlayerDestroyed(GameObject gObject)
        {
            StartCoroutine(Explosion(gObject.transform.position));
            player.GetComponent<BoxCollider2D>().enabled = false;
            player.enabled = false;
            playerAnim.Play("Invisible");
            player.Velocity = Vector3.zero;
            gObject.transform.rotation = Quaternion.identity;
            gObject.transform.position = new Vector3(0,0,1);
            StartCoroutine(PlayerRespawn(gObject.transform.position));
        }

        private void GameOver(GameObject gObject)
        {
            Destroy(player.gameObject);
        }

        private IEnumerator PlayerRespawn(Vector3 playerPosition)
        {
            yield return new WaitForSeconds(1.5f);
            if (player != null)
            {
                player.enabled = true;
                player.GetComponentInChildren<ParticleSystem>().Stop();
                player.GetComponent<Animator>().Play("Blink");
                yield return new WaitForSeconds(1f);
                player.GetComponent<Animator>().Play("Idle");
                player.GetComponent<BoxCollider2D>().enabled = true;
            }
        }


        private IEnumerator Explosion(Vector3 position)
        {
            var exp = Instantiate(PartExplosion, position, Quaternion.identity) as GameObject;
            exp.GetComponent<ParticleSystem>().Play();
            yield return new WaitForSeconds(0.5f);
            Destroy(exp);
        }

        private void BigUfoSpawn(GameObject gObject)
        {
            if (player == null) return;
            var u =
                Instantiate(BigU, Rand.CoinFlip() ? SpawnPoint1.transform.position : SpawnPoint2.transform.position,
                    Quaternion.identity) as GameObject;
            if (u != null) ufos.Add(u.GetComponent<Ufo>());
        }

        private void SmallUfoSpawn(GameObject gObject)
        {
            if (player == null) return;
            var u = Instantiate(SmallU, Rand.CoinFlip() ? SpawnPoint1.transform.position : SpawnPoint2.transform.position, Quaternion.identity) as GameObject;
            if (u != null) ufos.Add(u.GetComponent<Ufo>());
        }

        private void OnStart(GameObject gObject)
        {
            Debug.Log("ev " + ++counter);
            GetPlayer();
            StartCoroutine(PlayerRespawn(new Vector3(0,0,1)));
            asteroids.ForEach(asteroid => Destroy(asteroid.gameObject));
            asteroids.Clear();
        }

        private void GetPlayer()
        {
            var playerObject = Instantiate(playerPrefab, new Vector3(0, 0, 1), Quaternion.identity) as GameObject;
            if (playerObject != null)
            {
                player = playerObject.GetComponent<PlayerProps>();
                playerAnim = playerObject.GetComponent<Animator>();
                playerBox = playerObject.GetComponent<BoxCollider2D>();
            }
            player.enabled = false;
            playerBox.enabled = false;
            playerAnim.Play("Invisible");
        }

        // Use this for initialization
        void Start () {
	
        }
	
        // Update is called once per frame
        void LateUpdate ()
        {
            if (asteroids.Count == 0 && ufos.Count == 0)
                asteroids.AddRange(PopulateFirstWave());
        }

        private int counter;
    }
}
