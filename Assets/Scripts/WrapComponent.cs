﻿using UnityEngine;

namespace Assets.Scripts
{
    public class WrapComponent : MonoBehaviour
    {
        void Update()
        {
            if (transform.position.x > Camera.main.MaxX() + 1f)
                transform.position = new Vector3(Camera.main.MinX() - 1f, transform.position.y,
                    transform.position.z);

            if (transform.position.y > Camera.main.MaxY() + .5f)
                transform.position = new Vector3(transform.position.x, Camera.main.MinY() - 0.5f,
                    transform.position.z);

            if (transform.position.x < Camera.main.MinX() - 1f)
                transform.position = new Vector3(Camera.main.MaxX() + 1f, transform.position.y,
                    transform.position.z);

            if (transform.position.y < Camera.main.MinY() - .5f)
                transform.position = new Vector3(transform.position.y, Camera.main.MaxY() + .5f,
                    transform.position.z);
        }
    }
}
