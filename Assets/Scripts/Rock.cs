﻿using System;
using UnityEngine;
using System.Collections;
using Assets.Scripts;

public enum RockType
{
    Small,
    Medium,
    Big
}
public class Rock : MonoBehaviour
{
    public float Acceleration = 2;
    public RockType RockType;
    public Vector3 Velocity { get { return Acceleration*transform.up;} }
    public Vector3 colliderHit;
    private Transform MediumRock;


    public void Init(RockType rockType)
    {
        RockType = rockType;
    }

    void Awake()
    {
      
    }

    void FixedUpdate()
    {
        transform.position += Velocity * Time.fixedDeltaTime;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log("Hit");
        if (collider.gameObject.GetComponent<ShotProp>() != null)
        {
            OnRockDestroy();
            colliderHit = collider.gameObject.transform.right;
            Divide();
            Destroy(gameObject);
        }
    }

    void Divide()
    {
        for (int i = 0; i < 2; i++)
        {
            var  rock = Instantiate(MediumRock, gameObject.transform.position, Quaternion.identity) as Transform;
            rock.up = i == 0 ? colliderHit : colliderHit*-1;
        }
    }

    private void OnRockDestroy()
    {
        if (RockDestroyed != null)
            RockDestroyed.Invoke(gameObject);
    }

    public event Action<GameObject> RockDestroyed;
}
