﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;

public enum Message
{
    RockDestroyed,
    ShipDestroyed,
    PlayerDestroyed,
    ShotDestroyed,
    ShotFired,
    GameOver,
    BigUfoSpawn,
    SmallUfoSpawn,
    UfoDestroyed,
    GameStarted,
    GameEnded
}

public class EventDispatcher 
{
    private static EventDispatcher instance;

    public static EventDispatcher Instance
    {
        get { return instance ?? (instance = new EventDispatcher()); }
    }

    public readonly Dictionary<Message, List<Action<GameObject>>> eventMapper = new Dictionary<Message, List<Action<GameObject>>>();

    public void AddListener(Action<GameObject> invokedOnEvent, Message mappedMessage)
    {
        List<Action<GameObject>> l;
        if (eventMapper.TryGetValue(mappedMessage, out l))
            l.Add(invokedOnEvent);
        else
            eventMapper.Add(mappedMessage, new List<Action<GameObject>> {invokedOnEvent});
    }

    public void RaiseEvent(Message message, GameObject gObject)
    {
        List<Action<GameObject>> l;
        if (eventMapper.TryGetValue(message, out l))
            l.ForEach(a => a.Invoke(gObject));
    }

}
