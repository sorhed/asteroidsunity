﻿using UnityEngine;

namespace Assets.Scripts
{
    public class MenuControl : MonoBehaviour {

        [SerializeField] private Animator menuPanelAnim, scorePanelAnim;
        [SerializeField] private GameObject menuPanel, scorePanel;
        private bool hackCaller; //OnGameStarted is called during random period of time whenever player hits space. I cannot find the reason why so this is hack to find my way around it. It is ugly and i hate it.

        void Awake()
        {
            EventDispatcher.Instance.AddListener(x => hackCaller = false, Message.GameOver);
            EventDispatcher.Instance.AddListener(x => StartCoroutine(CorHelper.PopIn(menuPanelAnim,menuPanel)), Message.GameEnded);
        }

        public void OnGameStarted()
        {
            if (hackCaller) return;
            StartCoroutine(CorHelper.PopOut(menuPanelAnim, menuPanel));
            EventDispatcher.Instance.RaiseEvent(Message.GameStarted, null);
            hackCaller = true;
        }

        public void OnGoToScoreBoard()
        {
            StartCoroutine(CorHelper.PopOut(menuPanelAnim, menuPanel));
            StartCoroutine(CorHelper.PopIn(scorePanelAnim, scorePanel));
        }

        public void OnScoreReturn()
        {
            StartCoroutine(CorHelper.PopOut(scorePanelAnim, scorePanel));
            StartCoroutine(CorHelper.PopIn(menuPanelAnim, menuPanel));
        }

        public void GameEnded(GameObject gObject)
        {
            StartCoroutine(CorHelper.PopIn(menuPanelAnim, menuPanel));
        }
    }
}
