﻿using System;
using System.Runtime.Remoting.Channels;
using UnityEngine;

namespace Assets.Scripts
{
    public enum UfoType
    {
        Big,
        Small
    }
    public class Ufo : MonoBehaviour
    {
        public float Acceleration;
        public Vector3 Direction;
        public UfoType UType;
        private float shotCounter, turnCounter;
        private ShotComponent shot;
        private BoxCollider2D boxCol;
        private Transform playerPos;
        private float collInnerDistance;
        private bool isFlyingStraight;

        private Transform PlayerPos { get { return playerPos ?? (playerPos = FindObjectOfType<PlayerProps>().transform);} }
        private float TurnTimer
        {
            get
            {
                var turnTimer = 2f;
                return UType == UfoType.Small ? turnTimer*.7f : turnTimer;
            }
        }
        private float ShotTimer
        {
            get
            {
                var shotTimer = 1f;
                return UType == UfoType.Small ? shotTimer*.7f : shotTimer;
            }
        }
        private Vector3 DirectionTowardsPlayer { get {
                var h = PlayerPos.position - transform.position;
                return h / h.magnitude;
            } }

        public int DirectionModifyier;


        void Awake()
        {
            shot = GetComponent<ShotComponent>();
            boxCol = GetComponent<BoxCollider2D>();
            Direction = Vector3.right;
            collInnerDistance = (float) (Math.Sqrt(Math.Pow(boxCol.size.x, 2) + Math.Pow(boxCol.size.y, 2)) / 2) + .1f;
            DirectionModifyier = Rand.CoinFlip() ? -1 : 1;
        }

        // Use this for initialization
        void Start () {
	
        }

        void Update()
        {
            shotCounter += Time.deltaTime;
            turnCounter += Time.deltaTime;

            if (shotCounter > ShotTimer)
            {
                Shot();
                shotCounter = 0;
            }

            if (turnCounter > TurnTimer)
            {
                isFlyingStraight = !isFlyingStraight;
                turnCounter = 0;
            }

            Direction = (isFlyingStraight ? Vector3.right : new Vector3(1, .5f)) *DirectionModifyier;
            transform.position += Acceleration * Direction * Time.deltaTime;
        }
	
        // Update is called once per frame
        void FixedUpdate ()
        {
            
        }

        private void Shot()
        {
            switch (UType)
            {
                    case UfoType.Big:                   
                        var randDir = new Vector3((float)Rand.RandomDouble(-1,1), (float)Rand.RandomDouble(-1,1),0).normalized;
                        shot.OnFire(transform.position + collInnerDistance * randDir, transform.rotation, randDir);
                    break;
                    case UfoType.Small:
                        shot.OnFire(transform.position + collInnerDistance * DirectionTowardsPlayer, transform.rotation, DirectionTowardsPlayer);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }


        void OnTriggerEnter2D(Collider2D coll)
        {
            if (coll.gameObject.GetComponent<ShotProp>() || coll.gameObject.GetComponent<Asteroid>() ||
                coll.gameObject.GetComponent<PlayerProps>())
            {
                Destroy(gameObject);
                EventDispatcher.Instance.RaiseEvent(Message.UfoDestroyed, gameObject);
            }
        }
    }
}
