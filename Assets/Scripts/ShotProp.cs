﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    public class ShotProp : MonoBehaviour
    {
        public float Acceleration;
        public float MaxDistance = 4;
        private float traveledDistance;
        public Vector3 OldPos;
        private Vector3 direction;

        void Awake()
        {
            OldPos = transform.position;
        }

        public void Init(Vector3 direction)
        {
            this.direction = direction;
        }

        void Update()
        {
            transform.position += Acceleration*direction*Time.deltaTime;

            traveledDistance += (OldPos - transform.position).magnitude;
            OldPos = transform.position;

            if(traveledDistance > MaxDistance)
                Destroy(gameObject);
        }

        void OnTriggerEnter2D(Collider2D collider)
        {
                Destroy(gameObject);
        }
    }
}
