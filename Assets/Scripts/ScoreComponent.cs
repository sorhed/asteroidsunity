﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    public class ScoreComponent : MonoBehaviour
    {
        public int Score;
        public int Lives;
        private int smallUfoTreshold;
        private int bigUfoTreshold;

        void Awake()
        {
            EventDispatcher.Instance.AddListener(RockDestroyed, Message.RockDestroyed);
            EventDispatcher.Instance.AddListener(PlayerDestroyed, Message.PlayerDestroyed);
            EventDispatcher.Instance.AddListener(UfoDestroyed, Message.UfoDestroyed);
            EventDispatcher.Instance.AddListener(GameEnded, Message.GameEnded);

        }

        void LateUpdate()
		{
			if (bigUfoTreshold > 1500 && Score < 3500) {
				OnUfoCreated (UfoType.Big);
				bigUfoTreshold = 0;
			}
			else if (smallUfoTreshold > 1700 && Score > 3500) {
				OnUfoCreated (UfoType.Small);
				smallUfoTreshold = 0;
			}
		}

        private void RockDestroyed(GameObject gObject)
        {
            if (gObject.GetComponent<Asteroid>() != null)
            {
                var rockType = gObject.GetComponent<Asteroid>().RockType;
				//lazy and hardcoded but fuck it, only couple of objects are to be taken into consideration
                switch (rockType)
                {
                      case RockType.Big:
                        Score += 50;
                        bigUfoTreshold += 50;
                        smallUfoTreshold += 50;
                      break;
                      case RockType.Medium:
                        Score += 80;
                        bigUfoTreshold += 80;
                        smallUfoTreshold += 80;
                        break;
                      case RockType.Small:
                        Score += 120;
                        bigUfoTreshold += 120;
                        smallUfoTreshold += 120;
                        break;
                }
            }
        }

        private void UfoDestroyed(GameObject gObject) 
		{
			var ufoType = gObject.GetComponent<Ufo>().UType;
			switch (ufoType)
            {
                case UfoType.Big:
                    Score += 160;
                    bigUfoTreshold += 160;
                    smallUfoTreshold += 160;
                    break;
                case UfoType.Small:
                    Score += 200;
                    bigUfoTreshold += 200;
                    smallUfoTreshold += 200;
                    break;
            }
		}

        private void PlayerDestroyed(GameObject gObject)
        {
            if (Lives > 0)
                Lives -= 1;
            else
                OnGameOver();
        }

        private void GameEnded(GameObject gObject)
        {
            Score = 0;
            Lives = 3;
        }

        private void OnGameOver()
        {
            EventDispatcher.Instance.RaiseEvent(Message.GameOver, null);
        }

        private void OnUfoCreated(UfoType uType)
        {
            EventDispatcher.Instance.RaiseEvent(uType == UfoType.Big ? Message.BigUfoSpawn : Message.SmallUfoSpawn, null);
        }


    }
}
