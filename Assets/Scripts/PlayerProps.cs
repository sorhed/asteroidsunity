﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts
{
    public class PlayerProps : MonoBehaviour
    {

        public float Acceleration = 3;
        public float Friction = .99f;
        public Vector3 Velocity;
        public float RotationSpeed = 100;
        private ParticleSystem part;
        private ShotComponent shotComponent;

        void Awake()
        {
            shotComponent = GetComponent<ShotComponent>();
            part = GetComponentInChildren<ParticleSystem>();
            part.Stop();
        }

        void FixedUpdate()
        {
            transform.position += Velocity*Time.fixedDeltaTime;
            Velocity *= Friction;
        }

        void Update()
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                Velocity += Acceleration*transform.up*Time.deltaTime;
                StartCoroutine(Thrust());
            }
            if (Input.GetKey(KeyCode.RightArrow))
                transform.Rotate(Vector3.back, RotationSpeed * Time.deltaTime);
            if (Input.GetKey(KeyCode.LeftArrow))
                transform.Rotate(Vector3.forward, RotationSpeed * Time.deltaTime);
            if (Input.GetKeyDown(KeyCode.Space))
                shotComponent.OnFire((transform.localPosition + (GetComponent<BoxCollider2D>().size.y / 2 + .2f) * transform.up), transform.rotation, transform.up);
                
        }

        void OnTriggerEnter2D(Collider2D coll)
        {
            if (coll.gameObject.GetComponent<ShotProp>() != null || coll.gameObject.GetComponent<Asteroid>() != null)
            {
                OnPlayerDestroyed();
            }
        }

        private IEnumerator Thrust()
        {
            if (!enabled) yield break;
            part.Play();
            yield return new WaitUntil(() => Input.GetKeyUp(KeyCode.UpArrow));
            part.Stop();
        }


        private void OnPlayerDestroyed()
        {
            EventDispatcher.Instance.RaiseEvent(Message.PlayerDestroyed, gameObject);
        }


    }
}
