﻿using UnityEngine;

namespace Assets.Scripts
{
    public class ShotComponent : MonoBehaviour
    {
        [SerializeField] private GameObject shot;

        public void OnFire(Vector3 position, Quaternion rotation, Vector3 direction)
        {
           var s = Instantiate(shot, position, rotation) as GameObject;
           if (s != null) s.GetComponent<ShotProp>().Init(direction);
        }
    }
}
