﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class GetScores : MonoBehaviour
    {
        private Text content;

        void Awake()
        {
            content = gameObject.GetComponent<Text>();
        }

        void OnEnable()
        {
            var scores = File.ReadAllLines(Application.dataPath + "/HighScores/scores.txt").ToList();
            var hscores = new List<HighScore>();
            scores.ForEach(s => hscores.Add(JsonUtility.FromJson<HighScore>(s)));
            hscores = hscores.OrderByDescending(hs => Convert.ToInt32(hs.ScoreValue)).ToList();
            var hscoresArr = new string[hscores.Count];
            int i = 0;
            foreach (var hscore in hscores)
            {
                hscoresArr[i++] = hscore.ToString();
            }

            content.text = string.Join(Environment.NewLine, hscoresArr);
        }
	
    }
}

