﻿using System;
using System.Collections;
using System.Diagnostics;
using UnityEditor.Animations;
using UnityEngine;

namespace Assets.Scripts
{
    public static class CorHelper
    {
        public static IEnumerator PopIn(Animator anim, GameObject panel)
        {
            panel.SetActive(true);
            anim.Play("PopIn");
            yield return new WaitForSeconds(0.6f);
        }

        public static IEnumerator PopOut(Animator anim, GameObject panel)
        {
            anim.Play("PopOut");
            yield return new WaitForSeconds(0.6f);
            panel.SetActive(false);
        }

    }
}
